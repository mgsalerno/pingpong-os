#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppos_data.h"
#include "ppos_disk.h"
#include "ppos.h"

/* ----------------------------------------------------------------------- */

void dispatcher_body ();

task_t *scheduler ();

void handler ();

/* ----------------------------------------------------------------------- */

extern diskRequest_t *diskQueue;

task_t *tCurrent, *readyQueue, *sleepingQueue;	// Tarefa corrente, fila de tarefas prontas e fila de tarefas adormecidas
task_t tMain, tDispatcher;						// Tarefas main e dispatcher

int nTasks, preemption;							// ID das tarefas (único) e variável de controle de preempção
unsigned int systemTime;						// Relógio do sistema

struct sigaction action;						// Estrutura que define um tratador de sinal (deve ser global ou static)
struct itimerval timer;							// Estrutura de inicialização to timer


/* -----------------------------------------------------------------------
 * Inicializa o sistema operacional
 * Deve ser chamada no início do main()
 * ----------------------------------------------------------------------- */

void ppos_init ()
{
	#ifdef DEBUG
		printf ("ppos_init: iniciando o sistema\n");
	#endif

	setvbuf(stdout, 0, _IONBF, 0); // Desativa o buffer da saída padrão (stdout), usado pela função printf
	systemTime = 0;
	preemption = ON; // Liga a preempção
	nTasks = 0;
	readyQueue = sleepingQueue = NULL;

	tMain.initTime = systime();			//
	tMain.runTime = 0;					//
	tMain.id = nTasks++;				//
	tMain.next = tMain.prev = NULL;		//
	tMain.prioD = tMain.prioE = 0;		// Atribuições da tarefa Main
	tMain.type = USER_TASK;				//
	tMain.status = RUNNING;				//
	tMain.quantum = DEFAULT_QUANTUM;	//
	tMain.activations = 0;				//
	tMain.suspendedQueue = NULL;		//

	tCurrent = &tMain; // A primeira tarefa atual é a Main

	task_create(&tDispatcher, dispatcher_body, NULL); // Cria a tarefa dispatcher (id = 1)
	tDispatcher.type = SYSTEM_TASK;

	action.sa_handler = handler; // Registra a ação para o sinal de timer SIGALRM
	sigemptyset(&action.sa_mask);
	action.sa_flags = 0;

	if (sigaction (SIGALRM, &action, 0) < 0)
	{
		perror ("Erro em sigaction: ");
		exit (1);
	}

	timer.it_value.tv_usec = 1000;		// Primeiro disparo do temporizador, em micro-segundos
	timer.it_value.tv_sec  = 0;			// Primeiro disparo do temporizador, em segundos
	timer.it_interval.tv_usec = 1000;	// Disparos subsequentes, em micro-segundos
	timer.it_interval.tv_sec  = 0;		// Disparos subsequentes, em segundos

	if (setitimer (ITIMER_REAL, &timer, 0) < 0) // Arma o temporizador ITIMER_REAL (vide man setitimer)
	{
		perror ("Erro em setitimer: ");
		exit (1);
	}
}


/* -----------------------------------------------------------------------
 * Cria uma nova tarefa
 * Retorna um ID > 0 ou erro
 * ----------------------------------------------------------------------- */

int task_create (task_t *task, void (*start_func)(void *), void *arg)
{
	if (!task)
		return -1;

	task->initTime = systime();			//
	task->runTime = 0;					//
	task->id = nTasks++;				//
	task->next = task->prev = NULL;		//
	task->prioD = task->prioE = 0;		// Atribuições da tarefa
	task->type = USER_TASK;				//
	task->status = RUNNING;				//
	task->quantum = DEFAULT_QUANTUM;	//
	task->activations = 0;				//
	task->suspendedQueue = NULL;		//

	char *stack;

	getcontext(&(task->context)); // Salva o contexto atual na variável task->context

	stack = malloc(STACKSIZE);

	if (stack)
	{
		(task->context).uc_stack.ss_sp = task->stack = stack;	//
		(task->context).uc_stack.ss_size = STACKSIZE;			// Ajusta a pilha do contexto
		(task->context).uc_stack.ss_flags = 0;					//
		(task->context).uc_link = 0;							//
	}
	else
		return -1;

	makecontext(&(task->context), (void *)(*start_func), 1, arg); // Ajusta alguns valores internos do contexto salvo em task->context

	if (task != &tDispatcher)
		queue_append((queue_t **) &readyQueue, (queue_t *) task);

	#ifdef DEBUG
		printf ("task_create: criou tarefa %d\n", task->id);
	#endif

	return (task->id);
}


/* -----------------------------------------------------------------------
 * Termina a tarefa corrente, indicando um valor de status encerramento
 * ----------------------------------------------------------------------- */

void task_exit (int exitCode)
{
	#ifdef DEBUG
		printf ("task_exit: tarefa %d sendo encerrada\n", tCurrent->id);
	#endif

	printf("Task %d exit: execution time %d ms, processor time %5d ms, %4d activations\n", task_id(), systime() - tCurrent->initTime, tCurrent->runTime, tCurrent->activations);

	tCurrent->status = TERMINATED;
	tCurrent->exitCode = exitCode;

	task_t *aux;

	while (queue_size((queue_t *) tCurrent->suspendedQueue) > 0)
	{
		aux = (task_t *) queue_remove((queue_t **) &(tCurrent->suspendedQueue), (queue_t *) tCurrent->suspendedQueue); // Remove da fila de suspensas
		queue_append((queue_t **) &readyQueue, (queue_t *) aux); // Insere na fila de prontas
		aux->status = RUNNING;

		#ifdef DEBUG
			printf ("task_exit: retornando tarefa %d à fila de prontas (estava na lista de suspensas de %d)\n", aux->id, tCurrent->id);
		#endif
	}

	if (tCurrent == &tDispatcher)
	{
		free(tDispatcher.stack);
		task_switch(&tMain); // Volta para a tarefa main
	}
	else
		task_switch(&tDispatcher); // Volta para a tarefa dispatcher
}


/* -----------------------------------------------------------------------
 * Alterna a execução para a tarefa indicada
 * ----------------------------------------------------------------------- */

int task_switch (task_t *task)
{
	if (!task)
		return -1;

	task_t *tTemp = tCurrent; // Salva a tarefa corrente
	tCurrent = task; // Atualiza a tarefa corrente

	#ifdef DEBUG
		printf ("task_switch: trocando contexto %d -> %d\n", tTemp->id, task->id);
	#endif

	task->activations++;

	if (swapcontext(&(tTemp->context), &(task->context)) < 0) // Salva o contexto atual em tTemp->context e restaura o contexto salvo anteriormente em task->context
	{
		tCurrent = tTemp; // Restaura a tarefa corrente anterior
		return -1;
	}

	return 0;
}


/* -----------------------------------------------------------------------
 * Retorna o identificador da tarefa corrente (main deve ser 0)
 * ----------------------------------------------------------------------- */

int task_id ()
{
	return (tCurrent->id);
}


/* -----------------------------------------------------------------------
 * Libera o processador para a próxima tarefa,
 * retornando à fila de tarefas prontas ("ready queue")
 * ----------------------------------------------------------------------- */

void task_yield ()
{
	#ifdef DEBUG
		printf ("task_yield: a tarefa %d liberou o processador\n", tCurrent->id);
	#endif
	
	if (task_id() != 1) // Se a tarefa não é o dispatcher
		queue_append((queue_t **) &readyQueue, (queue_t *) tCurrent); // Devolve a tarefa no final da lista de prontas

	task_switch(&tDispatcher); // Transfere o controle para a tarefa dispatcher
}


/* -----------------------------------------------------------------------
 * Estrutura da tarefa Dispatcher
 * ----------------------------------------------------------------------- */

void dispatcher_body () // Dispatcher é uma tarefa
{
	while (queue_size((queue_t *) readyQueue) > 0 || queue_size((queue_t *) sleepingQueue) > 0 || queue_size((queue_t *) diskQueue)) // Enquanto existir algo na fila de prontas ou na fila de suspensas
	{
		if (readyQueue)
		{
			task_t *next = scheduler(); // scheduler é uma função
			if (next)
			{
				next->quantum = DEFAULT_QUANTUM; // Restaura o valor de quantum
				queue_remove((queue_t **) &readyQueue, (queue_t *) next);
				task_switch(next); // Transfere controle para a tarefa "next"

				if (next->status == TERMINATED) // Verifica se a tarefa já encerrou
				{
					#ifdef DEBUG
						printf ("dispatcher_body: liberando tarefa %d da memória\n", next->id);
					#endif

					free(next->stack);
				}
			}
		}

		if (sleepingQueue)
		{
			task_t *subsequent, *aux = sleepingQueue;

			do
			{
				if (aux->wakeUpTime <= systime()) // Se for a hora de acordar
				{
					#ifdef DEBUG
						printf ("dispatcher_body: acordando tarefa %d\n", aux->id);
					#endif

					preemption = OFF; // Desliga a preempção
					subsequent = aux->next; // Salva o sucessor para não perder a referência
					aux->status = RUNNING;
					queue_append((queue_t **) &readyQueue, queue_remove((queue_t **) &sleepingQueue, (queue_t *) aux)); // Retira da fila de suspensas e insere na fila de prontas
					aux = subsequent; // Retorna a referência
					preemption = ON; // Liga a preempção
				}
				else
					aux = aux->next;
			}
			while (sleepingQueue && aux != sleepingQueue); // Enquanto existir uma fila de suspensas e não voltar ao início da fila
		}
	}

	task_exit(0); // Encerra a tarefa dispatcher
}


/* -----------------------------------------------------------------------
 * Estrutura da funcao scheduler
 * Implementa a politica de envelhecimento
 * ----------------------------------------------------------------------- */

task_t *scheduler ()
{
	if (!readyQueue)
		return NULL;

	task_t *aux, *max;
	max = aux = readyQueue;

	do
	{
		aux->prioD += ALPHA; // Envelhecimento

		if (max->prioD > aux->prioD) // Encontra a tarefa de maior prioridade dinâmica
			max = aux;

		aux = aux->next;
	}
	while (aux != readyQueue);

	#ifdef DEBUG
		printf ("scheduler: próxima tarefa = %d (prioridade %d)\n", max->id, max->prioD);
	#endif

	max->prioD = max->prioE; // Restaura a prioridade estática da tarefa

	return max;
}


/* -----------------------------------------------------------------------
 * Função que ajusta a prioridade estática e dinâmica da tarefa task
 * para o valor prio (que deve estar entre -20 e +20)
 * ----------------------------------------------------------------------- */ 

void task_setprio (task_t *task, int prio)
{
	if (prio < -20 || prio > 20)
	{
		fprintf(stderr, "Erro: Valor da prioridade deve estar entre -20 e +20\n");
		return;
	}

	if (!task) // Caso a tarefa seja nula, usa a corrente
		task = tCurrent;

	#ifdef DEBUG
		printf ("task_setprio: ajustando a prioridade da tarefa %d para %d\n", task->id, prio);
	#endif

	task->prioD = task->prioE = prio; // Ajusta as prioridades
}


/* -----------------------------------------------------------------------
 * Devolve o valor da prioridade estática da tarefa task
 * (ou da tarefa corrente, se task for nulo)
 * ----------------------------------------------------------------------- */ 

int task_getprio (task_t *task)
{
	if (!task) // Caso a tarefa seja nula, usa a corrente
		task = tCurrent;

	#ifdef DEBUG
		printf ("task_getprio: retornando a prioridade da tarefa %d (%d)\n", task->id, task->prioE);
	#endif

	return (task->prioE);
}


/* -----------------------------------------------------------------------
 * Rotina de tratamento de ticks
 * ----------------------------------------------------------------------- */ 

void handler ()
{
	systemTime++;
	tCurrent->runTime++;

	if (tCurrent->type && preemption == ON) // Se for uma tarefa de usuário e puder realizar preempções
	{
		if (tCurrent->quantum) // Se ainda tiver ticks
			tCurrent->quantum--;
		else
			task_yield(); // Volta para o dispatcher
	}
}


/* -----------------------------------------------------------------------
 * Informa o valor corrente do relógio
 * ----------------------------------------------------------------------- */ 

unsigned int systime ()
{
	return systemTime;
}


/* -----------------------------------------------------------------------
 * A tarefa corrente aguarda o encerramento de outra task
 * ----------------------------------------------------------------------- */ 

int task_join (task_t *task)
{
	preemption = OFF; // Desliga a preempção

	if (!task || task->status == TERMINATED)
		return -1;

	#ifdef DEBUG
		printf ("task_join: a tarefa %d está esperando a tarefa %d terminar\n", task_id(), task->id);
	#endif

	queue_append((queue_t **) &task->suspendedQueue, (queue_t *) tCurrent); // Insere na lista de suspensas
	tCurrent->status = SUSPENDED;
	preemption = ON; // Liga a preempção
	task_switch(&tDispatcher);

	return (task->exitCode);
}


/* -----------------------------------------------------------------------
 * Suspende a tarefa corrente por t milissegundos
 * ----------------------------------------------------------------------- */ 

 void task_sleep (int t)
 {
	#ifdef DEBUG
		printf ("task_sleep: a tarefa %d será suspensa por %d ms\n", task_id(), t);
	#endif

 	preemption = OFF; // Desliga a preempção
 	tCurrent->status = SUSPENDED;
 	tCurrent->wakeUpTime = systime() + t; // Calcula o instante em que a tarefa deve ser acordada
 	queue_append((queue_t **) &sleepingQueue, (queue_t *) tCurrent);
 	preemption = ON; // Liga a preempção
 	task_switch(&tDispatcher);
 }


/* -----------------------------------------------------------------------
 * Cria um semáforo com valor inicial "value"
 * ----------------------------------------------------------------------- */ 

int sem_create (semaphore_t *s, int value)
{
	preemption = OFF;

	if (!s)
	{
		preemption = ON;
		return -1;
	}

	#ifdef DEBUG
		printf ("sem_create: inicializando semáforo com valor inicial = %d\n", value);
	#endif

	s->flag = ON;			// 
	s->counter = value;		// Atribuições do semáforo
	s->waitingQueue = NULL;	// 
	preemption = ON;
	return 0;
}


/* -----------------------------------------------------------------------
 * Requisita o semáforo
 * ----------------------------------------------------------------------- */ 

int sem_down (semaphore_t *s)
{
	preemption = OFF;

	if (!s || s->flag == OFF) // Semáforo não existe ou foi destruído
	{
		preemption = ON;
		return -1;
	}

	s->counter--; // Decrementa o contador do semáforo

	if (s->counter < 0)
	{
		#ifdef DEBUG
			printf ("sem_down: suspendendo tarefa %d\n", tCurrent->id);
		#endif

		tCurrent->status = SUSPENDED;
		queue_append((queue_t **) &(s->waitingQueue), (queue_t *) tCurrent); // Insere na lista de espera do semáforo
		preemption = ON;
		task_switch(&tDispatcher);
	}

	preemption = ON;
	return 0;
}


/* -----------------------------------------------------------------------
 * Libera o semáforo
 * ----------------------------------------------------------------------- */ 

int sem_up (semaphore_t *s)
{
	preemption = OFF;

	if (!s || s->flag == OFF)
	{
		preemption = ON;
		return -1;
	}

	s->counter++; // Incrementa o contador do semáforo
	task_t *aux;

	if (s->counter <= 0 && s->waitingQueue)
	{
		aux = (task_t *) queue_remove((queue_t **) &(s->waitingQueue), (queue_t *) (s->waitingQueue)); // Retira da fila do semáforo
		aux->status = RUNNING;
		queue_append((queue_t **) &readyQueue, (queue_t *) aux); // Insere na fila de prontas

		#ifdef DEBUG
			printf ("sem_up: acordando tarefa %d\n", aux->id);
		#endif
	}

	preemption = ON;
	return 0;
}


/* -----------------------------------------------------------------------
 * Destroi o semáforo, liberando as tarefas bloqueadas
 * ----------------------------------------------------------------------- */ 

int sem_destroy (semaphore_t *s)
{
	preemption = OFF;

	if (!s || s->flag == OFF)
	{
		preemption = ON;
		return -1;
	}

	#ifdef DEBUG
		printf ("sem_destroy: acordando tarefas que esperavam pelo semáforo\n");
	#endif

	task_t *subsequent, *aux = s->waitingQueue;

	if (aux)
	{
		do
		{
			subsequent = aux->next; // Salva o sucessor para não perder a referência
			aux->status = RUNNING;
			queue_append((queue_t **) &readyQueue, queue_remove((queue_t **) &(s->waitingQueue), (queue_t *) aux)); // Retira da fila do semáforo e insere na fila de prontas
			aux = subsequent; // Retorna a referência
		}
		while (s->waitingQueue); // Enquanto existir algo na fila
	}

	s->flag = OFF; // Marca o semáforo como destruído
	preemption = ON;
	return 0;
}


/* -----------------------------------------------------------------------
 * Cria uma fila para até max mensagens de size bytes cada
 * ----------------------------------------------------------------------- */

int mqueue_create (mqueue_t *queue, int max, int size)
{
	if (!queue || max <= 0 || size <= 0)
		return -1;

	#ifdef DEBUG
		printf("mqueue_create: criando fila de mensagens\n");
	#endif

	if (!(queue->buffer = malloc(max * size)))
		return -1;

	queue->status = RUNNING;
	queue->start = queue->end = queue->num_msgs = 0;
	queue->max_msgs = max;
	queue->msg_size = size;

	if (sem_create(&(queue->s_buffer), 1) < 0 || sem_create(&(queue->s_vaga), max) < 0 || sem_create(&(queue->s_msg), 0) < 0) // Verifica a criação dos semáforos
		return -1;

	return 0;
}


/* -----------------------------------------------------------------------
 * Envia uma mensagem para a fila
 * ----------------------------------------------------------------------- */

int mqueue_send (mqueue_t *queue, void *msg)
{
	if (!queue || !msg || queue->status == TERMINATED)
		return -1;

	#ifdef DEBUG
		printf("mqueue_send: enviando mensagem\n");
	#endif

	sem_down(&(queue->s_vaga));

	sem_down(&(queue->s_buffer));
	memcpy(queue->buffer + (queue->end * queue->msg_size), msg, queue->msg_size); // Copia a mensagem da variável para o buffer
	queue->num_msgs++; // Incrementa o número de mensagens na fila
	queue->end = (queue->end + 1) % queue->max_msgs; // Atualiza a posição do fim do buffer
	sem_up(&(queue->s_buffer));

	sem_up(&(queue->s_msg));

	return 0;
}


/* -----------------------------------------------------------------------
 * Recebe uma mensagem da fila
 * ----------------------------------------------------------------------- */

int mqueue_recv (mqueue_t *queue, void *msg)
{
	if (!queue || !msg || queue->status == TERMINATED)
		return -1;

	#ifdef DEBUG
		printf("mqueue_recv: recebendo mensagem\n");
	#endif

	sem_down(&(queue->s_msg));

	sem_down(&(queue->s_buffer));
	memcpy(msg, queue->buffer + (queue->start * queue->msg_size), queue->msg_size); // Copia a mensagem do buffer para a variável
	queue->num_msgs--; // Decrementa o número de mensagens na fila
	queue->start = (queue->start + 1) % queue->max_msgs; // Atualiza a posição do início do buffer
	sem_up(&(queue->s_buffer));

	sem_up(&(queue->s_vaga));

	return 0;
}


/* -----------------------------------------------------------------------
 * Destroi a fila, liberando as tarefas bloqueadas
 * ----------------------------------------------------------------------- */

int mqueue_destroy (mqueue_t *queue)
{
	if (!queue || queue->status == TERMINATED)
		return -1;

	#ifdef DEBUG
		printf("mqueue_destroy: destruindo fila de mensagens\n");
	#endif

	if (sem_destroy(&(queue->s_buffer)) < 0 || sem_destroy(&(queue->s_vaga)) < 0 ||	sem_destroy(&(queue->s_msg)) < 0) // Verifica a destruição dos semáforos
		return -1;

	queue->status = TERMINATED; // Muda o status da fila para TERMINATED
	free(queue->buffer); // Libera o buffer

	return 0;
}


/* -----------------------------------------------------------------------
 * Informa o número de mensagens atualmente na fila
 * ----------------------------------------------------------------------- */

int mqueue_msgs (mqueue_t *queue)
{
	if (!queue || queue->status == TERMINATED)
		return -1;

	return (queue->num_msgs); // Retorna o número de mensagens na fila
}


/* ----------------------------------------------------------------------- */
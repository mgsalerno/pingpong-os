/* PingPongOS - PingPong Operating System --------------------------------
 * Prof. Carlos A. Maziero, DINF UFPR ------------------------------------
 * Versão 1.1 -- Julho de 2016 ------------------------------------------- */

/* -----------------------------------------------------------------------
 * Estruturas de dados internas do sistema operacional
 * ----------------------------------------------------------------------- */

#ifndef __PPOS_DATA__
#define __PPOS_DATA__

#include <ucontext.h>	// Biblioteca POSIX de trocas de contexto
#include <sys/time.h>	// Biblioteca de tempo
#include <signal.h>		// Biblioteca de sinais
#include "queue.h"		// Biblioteca de filas genéricas

#define STACKSIZE 32768       // Tamanho de pilha das threads
#define ALPHA -1              // Envelhecimento
#define DEFAULT_QUANTUM 20    // Valor padrão de quantum
#define SYSTEM_TASK 0         // Tarefas de sistema
#define USER_TASK 1           // Tarefas de usuário
#define SUSPENDED 2           // Status de tarefa suspensa
#define RUNNING 1             // Status de tarefa executando
#define TERMINATED 0          // Status de tarefa encerrada
#define ON 1                  // Variável ligada
#define OFF 0                 // Variável desligada

/* ----------------------------------------------------------------------- */

typedef struct task_t // Estrutura que define um Task Control Block (TCB)
{
   struct task_t *prev, *next ;     // Ponteiros para usar em filas
   int id ;                         // Identificador da tarefa
   ucontext_t context ;             // Contexto armazenado da tarefa
   void *stack ;                    // Aponta para a pilha da tarefa
   int status ;                     // Status da tarefa
   int prioE ;                      // Prioridade estática da tarefa
   int prioD ;                      // Prioridade dinâmica da tarefa
   int type ;                       // Tipo da tarefa
   int quantum ;                    // Quantidade de ticks
   int activations ;                // Número de ativações
   unsigned int initTime ;          // Tempo de inicialização
   unsigned int runTime ;           // Tempo de execução
   struct task_t *suspendedQueue ;  // Fila de tarefas esperando o que essa tarefa termine
   int exitCode ;                   // Código de encerramento da tarefa
   int wakeUpTime ;                 // Instante em que a tarefa deverá ser acordada
   // ... (outros campos serão adicionados mais tarde)
} task_t ;

/* -----------------------------------------------------------------------
 * Estrutura que define um semáforo
 * ----------------------------------------------------------------------- */

typedef struct
{
  int counter ;           // Contador do semáforo
  int flag ;              // Flag para verificar se o semáforo é válido ou não
  task_t *waitingQueue ;  // Fila de tarefas aguardando no semáforo
} semaphore_t ;


/* -----------------------------------------------------------------------
 * Estrutura que define um mutex
 * ----------------------------------------------------------------------- */

typedef struct
{
  // preencher quando necessário
} mutex_t ;


/* -----------------------------------------------------------------------
 * Estrutura que define uma barreira
 * ----------------------------------------------------------------------- */

typedef struct
{
  // preencher quando necessário
} barrier_t ;


/* -----------------------------------------------------------------------
 * Estrutura que define uma fila de mensagens
 * ----------------------------------------------------------------------- */

typedef struct
{
  semaphore_t s_buffer ; // Semáforo de controle de acesso ao buffer
  semaphore_t s_vaga ;   // Semáforo de controle de acesso às vagas do buffer
  semaphore_t s_msg ;    // Semáforo de controle de acesso às mensagens
  int msg_size ;         // Tamanho máximo das mensagens
  int max_msgs ;         // Quantidade máxima de mensagens
  int num_msgs ;         // Número de mensagens atualmente na fila
  int start ;            // Início do buffer circular
  int end ;              // Fim do buffer circular
  int status ;           // Status da fila de mensagens
  void *buffer ;         // Buffer de mensagens
} mqueue_t ;


#endif

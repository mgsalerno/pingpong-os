#include <stdio.h>
#include <stdlib.h>
#include "hard_disk.h"
#include "ppos_disk.h"
#include "queue.h"
#include "ppos.h"

/* ----------------------------------------------------------------------- */

void diskDriverBody (void *args) ;

void diskHandler () ;

/* ----------------------------------------------------------------------- */

extern task_t *tCurrent, *readyQueue, tDispatcher;	// Tarefa corrente, fila de prontas e tarefa Dispatcher (externas)

task_t *diskSuspendedQueue, tDiskManager;			// Fila de tarefas suspensas pelo disco e tarefa gerenciadora de disco

disk_t hardDisk;									// Disco

diskRequest_t *diskQueue;							// Fila de requisições ao disco

struct sigaction diskManagerHandler;				// Estrutura que define um tratador de sinal


/* -----------------------------------------------------------------------
 * Inicializacao do gerente de disco
 * retorna -1 em erro ou 0 em sucesso
 * numBlocks: tamanho do disco, em blocos
 * blockSize: tamanho de cada bloco do disco, em bytes
 * ----------------------------------------------------------------------- */

int disk_mgr_init (int *numBlocks, int *blockSize)
{
	#ifdef DEBUG
		printf ("disk_mgr_init: inicializando o disco\n");
	#endif

	diskQueue = NULL;
	diskSuspendedQueue = NULL;

	diskManagerHandler.sa_handler = diskHandler; // Registra a ação para o sinal SIGUSR1
	sigemptyset(&diskManagerHandler.sa_mask);
	diskManagerHandler.sa_flags = 0;

	if (sigaction (SIGUSR1, &diskManagerHandler, 0) < 0)
	{
		perror ("Erro em sigaction: ");
		exit (1);
	}

	task_create(&tDiskManager, diskDriverBody, NULL); // Cria a tarefa de gerenciamento de disco
	tDiskManager.type = SYSTEM_TASK;
	queue_append((queue_t **) &diskSuspendedQueue, queue_remove((queue_t **) &readyQueue, (queue_t *) &tDiskManager));
	tDiskManager.status = SUSPENDED;

	if (disk_cmd(DISK_CMD_INIT, 0, 0) < 0)
		return -1;

	if ((*numBlocks = disk_cmd(DISK_CMD_DISKSIZE, 0, 0)) < 0 || (*blockSize = disk_cmd(DISK_CMD_BLOCKSIZE, 0, 0)) < 0)
		return -1;

	if (sem_create(&(hardDisk.s_access), 1) < 0)
		return -1;

	return 0;
}


/* -----------------------------------------------------------------------
 * Leitura de um bloco, do disco para o buffer
 * ----------------------------------------------------------------------- */

int disk_block_read (int block, void *buffer)
{
	if (block < 0)
		return -1;

	if (sem_down(&(hardDisk.s_access)) < 0) // Obtém o semáforo de acesso ao disco
		return -1;

	diskRequest_t *taskRequest;

	if (!(taskRequest = (diskRequest_t *) malloc(sizeof(diskRequest_t))))
		return -1;

	taskRequest->next = taskRequest->prev = NULL;
	taskRequest->cmd = DISK_CMD_READ;
	taskRequest->block = block;
	taskRequest->buffer = buffer;
	taskRequest->requester = tCurrent;

	queue_append((queue_t **) &diskQueue, (queue_t *) taskRequest); // Inclui o pedido na fila_disco

	#ifdef DEBUG
		printf ("disk_block_read: requisição de leitura no bloco %d\n", block);
	#endif

	if (tDiskManager.status == SUSPENDED)
	{
		tDiskManager.status = RUNNING; // Acorda o gerente de disco
		queue_append((queue_t **) &readyQueue, queue_remove((queue_t **) &diskSuspendedQueue, (queue_t *) &tDiskManager)); // Põe ele na fila de prontas
	}

	if (sem_up(&(hardDisk.s_access)) < 0) // Libera semáforo de acesso ao disco
		return -1;

	queue_append((queue_t **) &diskSuspendedQueue, (queue_t *) tCurrent); // Suspende a tarefa corrente
	tCurrent->status = SUSPENDED;

	task_switch(&tDispatcher); // Retorna ao dispatcher

	return 0;
}


/* -----------------------------------------------------------------------
 * Escrita de um bloco, do buffer para o disco
 * ----------------------------------------------------------------------- */

int disk_block_write (int block, void *buffer)
{
	if (block < 0)
		return -1;

	if (sem_down(&(hardDisk.s_access)) < 0) // Obtém o semáforo de acesso ao disco
		return -1;

	diskRequest_t *taskRequest;

	if (!(taskRequest = (diskRequest_t *) malloc(sizeof(diskRequest_t))))
		return -1;

	taskRequest->next = taskRequest->prev = NULL;
	taskRequest->cmd = DISK_CMD_WRITE;
	taskRequest->block = block;
	taskRequest->buffer = buffer;
	taskRequest->requester = tCurrent;

	queue_append((queue_t **) &diskQueue, (queue_t *) taskRequest); // Inclui o pedido na fila_disco

	#ifdef DEBUG
		printf ("disk_block_write: requisição de escrita no bloco %d\n", block);
	#endif

	if (tDiskManager.status == SUSPENDED)
	{
		tDiskManager.status = RUNNING; // Acorda o gerente de disco
		queue_append((queue_t **) &readyQueue, queue_remove((queue_t **) &diskSuspendedQueue, (queue_t *) &tDiskManager)); // Põe ele na fila de prontas
	}

	if (sem_up(&(hardDisk.s_access)) < 0) // Libera semáforo de acesso ao disco
		return -1;

	queue_append((queue_t **) &diskSuspendedQueue, (queue_t *) tCurrent); // Suspende a tarefa corrente
	tCurrent->status = SUSPENDED;

	task_switch(&tDispatcher); // Retorna ao dispatcher

	return 0;
}


/* -----------------------------------------------------------------------
 * Estrutura da tarefa de gerenciamento de disco
 * ----------------------------------------------------------------------- */

void diskDriverBody (void *args)
{
	while (1)
	{
		sem_down(&(hardDisk.s_access)); // Obtém o semáforo de acesso ao disco

		if (hardDisk.signal == ON) // Se foi acordado devido a um sinal do disco
		{
			hardDisk.signal = OFF;

			#ifdef DEBUG
				printf ("diskDriverBody: sinal SIGUSR1 recebido\n");
			#endif

			diskRequest_t *next = (diskRequest_t *) queue_remove((queue_t **) &diskQueue, (queue_t *) diskQueue); // Escolhe na fila o pedido a ser atendido, usando FCFS
			next->requester->status = RUNNING;
			queue_append((queue_t **) &readyQueue, queue_remove((queue_t **) &diskSuspendedQueue, (queue_t *) next->requester)); // Acorda a tarefa cujo pedido foi atendido

			free(next);
		}

		if ((disk_cmd(DISK_CMD_STATUS, 0, 0) == DISK_STATUS_IDLE) && (diskQueue)) // Se o disco estiver livre e houver pedidos de E/S na fila
		{
			#ifdef DEBUG
				printf ("diskDriverBody: disco livre, efetuando operações E/S\n");
			#endif

			diskRequest_t *next = diskQueue; // Escolhe na fila o pedido a ser atendido, usando FCFS
			disk_cmd(next->cmd, next->block, next->buffer); // Solicita ao disco a operação de E/S, usando disk_cmd()
		}

		sem_up(&(hardDisk.s_access)); // Libera semáforo de acesso ao disco

		queue_append((queue_t **) &diskSuspendedQueue, (queue_t *) tCurrent); // Suspende a tarefa corrente
		tCurrent->status = SUSPENDED;

		task_switch(&tDispatcher); // Retorna ao dispatcher
	}
}


/* -----------------------------------------------------------------------
 * Rotina de tratamento de disco
 * ----------------------------------------------------------------------- */

void diskHandler ()
{
	hardDisk.signal = ON;

	if (tDiskManager.status == SUSPENDED)
	{
		tDiskManager.status = RUNNING; // Acorda o gerente de disco
		queue_append((queue_t **) &readyQueue, queue_remove((queue_t **) &diskSuspendedQueue, (queue_t *) &tDiskManager)); // Põe ele na fila de prontas
	}
}

/* ----------------------------------------------------------------------- */
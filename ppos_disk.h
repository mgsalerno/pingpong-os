/* PingPongOS - PingPong Operating System --------------------------------
 * Prof. Carlos A. Maziero, DINF UFPR ------------------------------------
 * Versão 1.2 -- Julho de 2017 ------------------------------------------- */

/* -----------------------------------------------------------------------
 * Interface do gerente de disco rígido (block device driver)
 * ----------------------------------------------------------------------- */

#ifndef __DISK_MGR__
#define __DISK_MGR__

#include "ppos_data.h"


/* -----------------------------------------------------------------------
 * Estruturas de dados e rotinas de inicializacao e acesso
 * a um dispositivo de entrada/saida orientado a blocos,
 * tipicamente um disco rigido.
 * ----------------------------------------------------------------------- */

/* -----------------------------------------------------------------------
 * Estrutura que representa uma requisição do disco
 * ----------------------------------------------------------------------- */

typedef struct diskRequest_t
{
	struct diskRequest_t *prev, *next ;
	int cmd ;
	int block ;
	void *buffer ;
	task_t *requester ;
} diskRequest_t ;


/* -----------------------------------------------------------------------
 * Estrutura que representa um disco no sistema operacional
 * ----------------------------------------------------------------------- */

typedef struct
{
	semaphore_t s_access ;
	int signal ;
} disk_t ;

/* -----------------------------------------------------------------------
 * Inicializacao do gerente de disco
 * retorna -1 em erro ou 0 em sucesso
 * numBlocks: tamanho do disco, em blocos
 * blockSize: tamanho de cada bloco do disco, em bytes
 * ----------------------------------------------------------------------- */

int disk_mgr_init (int *numBlocks, int *blockSize) ;


/* -----------------------------------------------------------------------
 * Leitura de um bloco, do disco para o buffer
 * ----------------------------------------------------------------------- */

int disk_block_read (int block, void *buffer) ;


/* -----------------------------------------------------------------------
 * Escrita de um bloco, do buffer para o disco
 * ----------------------------------------------------------------------- */

int disk_block_write (int block, void *buffer) ;


#endif

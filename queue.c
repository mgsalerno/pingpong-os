#include <stdio.h>
#include "queue.h"


/* -----------------------------------------------------------------------
 * Insere um elemento no final da fila.
 * Condicoes a verificar, gerando msgs de erro:
 * - a fila deve existir
 * - o elemento deve existir
 * - o elemento nao deve estar em outra fila
 * ----------------------------------------------------------------------- */

void queue_append (queue_t **queue, queue_t *elem)
{
	if (!queue)
	{
		fprintf(stderr, "Erro: Fila não existe\n");
		return;
	}

	if (!elem)
	{
		fprintf(stderr, "Erro: Elemento não existe\n");
		return;
	}

	if (elem->next || elem->prev)
	{
		fprintf(stderr, "Erro: Elemento já pertence à outra fila\n");
		return;
	}

	if (*queue) // Se existir algo na fila
	{
		queue_t *ultimo = (*queue)->prev;
		elem->next = (*queue);
		(*queue)->prev = elem;
		elem->prev = ultimo;
		ultimo->next = elem;
		return;
	}
	else // Se for fila vazia
	{
		(*queue) = elem;
		(*queue)->next = (*queue)->prev = elem;
		return;
	}
}


/* -----------------------------------------------------------------------
 * Remove o elemento indicado da fila, sem o destruir.
 * Condicoes a verificar, gerando msgs de erro:
 * - a fila deve existir
 * - a fila nao deve estar vazia
 * - o elemento deve existir
 * - o elemento deve pertencer a fila indicada
 * Retorno: apontador para o elemento removido, ou NULL se erro
 * ----------------------------------------------------------------------- */

queue_t *queue_remove (queue_t **queue, queue_t *elem)
{
	if (!queue)
	{
		fprintf(stderr, "Erro: Fila não existe\n");
		return NULL;
	}

	if (!queue_size(*queue))
	{
		fprintf(stderr, "Erro: Fila Vazia\n");
		return NULL;
	}

	if (!elem)
	{
		fprintf(stderr, "Erro: Elemento não existe\n");
		return NULL;
	}

	queue_t *aux = (*queue);

	while (aux->next != (*queue) && aux != elem)
		aux = aux->next;

	if (aux != elem)
	{
		fprintf(stderr, "Erro: Elemento não pertence à fila\n");
		return NULL;
	}

	if (aux == (*queue)) // Caso seja o inicio da lista
	{
		if (aux->next == aux) // Caso seja o unico elemento na lista
			(*queue) = NULL;
		else
			(*queue) = (*queue)->next;

	}

	elem->prev->next = elem->next;
	elem->next->prev = elem->prev;
	elem->next = elem->prev = NULL;
	return elem;
}


/* -----------------------------------------------------------------------
 * Conta o numero de elementos na fila
 * Retorno: numero de elementos na fila
 * ----------------------------------------------------------------------- */

int queue_size (queue_t *queue)
{
	int tamanho = 0;

	if (queue)
	{
		tamanho++;
		queue_t *aux = queue;

		while (aux->next != queue)
		{
			tamanho++;
			aux = aux->next;
		}
	}

	return tamanho;
}


/* -----------------------------------------------------------------------
 * Percorre a fila e imprime na tela seu conteúdo. A impressão de cada
 * elemento é feita por uma função externa, definida pelo programa que
 * usa a biblioteca.
 * Essa função deve ter o seguinte protótipo:
 * void print_elem (void *ptr) ; // ptr aponta para o elemento a imprimir
 * ----------------------------------------------------------------------- */

void queue_print (char *name, queue_t *queue, void print_elem (void*))
{
	printf("%s [ ", name);

	if (!queue)
	{
		printf("]\n");
		return;
	}

	queue_t *aux = queue;

	do
	{
		(*print_elem)(aux);
		printf(" ");
		aux = aux->next;
	}
	while (aux != queue);

	printf("]\n");
	return;
}
